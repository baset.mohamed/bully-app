using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
public class bully_process
{
    private int ipid;
    public int pid
    {
        get { return ipid; }
    }
    private bool bismaster;
    public bool ismaster
    {
        get { return bismaster; }
        set { bismaster = value; }
    }
    private int socket_receive_timeout;
    private List<int> peers_ports;
    private int my_port;
    private int manager_port; //This port is not related to the algorith, just the main process which logs and inform process to kill itself
    public bully_process(int ptimeout, int pmy_port, List<int> ppeers_ports, int pmanager_port)
    {
        socket_receive_timeout = ptimeout;
        my_port = pmy_port;
        peers_ports = ppeers_ports;
        manager_port = pmanager_port;
        Process currentProcess = Process.GetCurrentProcess();
        ipid = currentProcess.Id;
    }

    public static Socket ConnectSocket(string server, int port, bool is_server)
    {
        Socket s = null;
        IPHostEntry hostEntry = null;
        try
        {


            hostEntry = Dns.GetHostEntry("localhost");

            IPAddress ipAddress = hostEntry.AddressList[0];
            IPEndPoint ipe = new IPEndPoint(ipAddress, port);
            Socket tempSocket =
            new Socket(ipe.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            Console.WriteLine("Socket Was Created Successfully. Details: " + port.ToString() + ", " + (is_server ? "Receiver Socket" : "Sender Socket"));
            if (is_server)
                tempSocket.Bind(ipe);
            else
                tempSocket.Connect(ipe);
            return tempSocket;
        }
        catch (Exception e)
        {
            Console.WriteLine("An Exception occured while creating socket; Details: " + port.ToString() + ", " + (is_server ? "Receiver Socket" : "Sender Socket") + ", " + e.Message);
        }
        return s;
    }
    public bool elect_my_self()
    {
        //Send message to all other process with my pid
        bool Iam_master = true;
        for (int i = 0; i < peers_ports.Count; i++)
        {
            int peer_port = peers_ports[i];
            if (my_port == peer_port)
                continue;
            //Send a message to declare election of myseld for coordinator rule by sending my pid
            string request = "" + pid;
            Byte[] bytesSent = Encoding.ASCII.GetBytes(request);
            Byte[] bytesReceived = new Byte[256];
            try
            {
                Socket Sender = ConnectSocket("127.0.0.1", peer_port, false);
                if (Sender != null)
                {
                    Console.WriteLine("Sending..");
                    Sender.Send(bytesSent, bytesSent.Length, 0);
                    Sender.Shutdown(SocketShutdown.Both);
                    Sender.Close();
                }
                else
                    throw new Exception("Port is not opened yet");
            }
            catch (Exception ex)
            {
                Console.WriteLine("A problem occured when trying to send a message from " + my_port.ToString() + " to " + peer_port.ToString() +
                " which can be caused by intialization of ports. Error Details:" + ex.Message);
            }
        }
        return Iam_master;
    }
    public void Start()
    {
        Task.Run(() => listen_to_coordinator());

        DT = DateTime.Now;
        while (true)
        {
            if (ismaster || DateTime.Now - DT >= TimeSpan.FromSeconds(2))
            {
                Console.WriteLine("Reelecting Myself," + ismaster);
                ismaster = true;
                elect_my_self();

                System.Threading.Thread.Sleep(1000);
            }

        }
    }
    DateTime DT;
    bool is_exiting = false;
    public bool listen_to_coordinator()
    {

        // Create the sockets

        Socket S = ConnectSocket("127.0.0.1", my_port, true);
        S.Listen(10);
        Console.WriteLine("Waiting on " + my_port + "..");
        while (!is_exiting)
        {
            Socket ss = S.Accept();
            string data = null;
            byte[] bytes = null;

            bytes = new byte[1024];
            int bytesRec = ss.Receive(bytes);
            data += Encoding.ASCII.GetString(bytes, 0, bytesRec);
            Console.WriteLine("Received Pid on " + my_port + ", received pid=" + data);

            int client_pid = int.Parse(data);//get_pid(sender_pid);
            Console.WriteLine("pid=" + pid + ", client_pid=" + client_pid);
            if (pid < client_pid)
            {
                ismaster = false;
                DT = DateTime.Now;
            }
            ss.Shutdown(SocketShutdown.Both);
            ss.Close();

        }

        return true;

    }


    public bool kill()
    {
        //Kill the process
        return true;
    }


}