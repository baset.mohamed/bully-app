﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Runtime.InteropServices;

namespace Bully
{
    class Program
    {
        static void Main(string[] args)
        {
            //Multiple instance of this app will run , the main process will be the manager/logger node which has nothing to do with the 
            //algorithm other than send commands to start porcess, kill it or receive the process action to log it on screen
            //The nodes of the bully algorithm will be created using process (not threads) where they use sockets as their ipc (interprocess communication)
            //starting the nodes will be done by calling the same app instance but with different args where the option -node must be passed along with 
            //the list of ports of its peers, time out and number of processes
            int socket_receive_timeout = 0; // 1000 ms = 1 s
            List<int> ports = new List<int>();
            int num_of_processes = 0;
            int my_bully_node_port = -1;
            int the_manager_port = -1;
            ParseArgs(args, out socket_receive_timeout, out ports, out num_of_processes, out my_bully_node_port, out the_manager_port);
            if (my_bully_node_port == -1)//This is the manger process
            {
                //Just Create the processes and listen to them + Send them message to kill themselves as soon as you get an input from user

                //First Step: Create the number of processes
                StringBuilder process_args = new StringBuilder();
                process_args.Append("-n ");
                process_args.Append(num_of_processes);
                process_args.Append(" -p ");
                foreach (int port in ports)
                {
                    process_args.Append(port);
                    process_args.Append(" ");
                }
                process_args.Append(" -t ");
                process_args.Append(socket_receive_timeout);
                process_args.Append(" -m ");
                process_args.Append(the_manager_port);
                string myApplication = System.Reflection.Assembly.GetEntryAssembly().Location;

                for (int i = 0; i < num_of_processes; i++)
                {
                    StringBuilder my_process_args = new StringBuilder(process_args.ToString());
                    my_process_args.Append(" -b ");
                    my_process_args.Append(ports[i]);
                    ProcessStartInfo PSI = new ProcessStartInfo(/*myAppLocation, my_process_args.ToString()*/);
                    PSI.UseShellExecute = false;
                    PSI.CreateNoWindow = true;
                    if (OS.IsGnu() || OS.IsMac())
                    {
                        PSI.FileName = "/bin/bash";
                        PSI.Arguments = "-c \"sudo /usr/bin/dotnet " /*+ myApplication*/+ "/home/abdelbaset/Work/Interviews/c3o/bully-app/bin/Debug/netcoreapp5.0/publish/dotnetcore.dll" + " " + my_process_args.ToString() + "\"";
                        Console.WriteLine(PSI.Arguments);
                    }
                    else
                    {
                        PSI.FileName = myApplication;
                        PSI.Arguments = my_process_args.ToString();
                    }
                    Process.Start(PSI);
                }
                Console.WriteLine("Welcome to the bully Algorithm. If you want to kill a process, please type pid pr port number:");
            }
            else //This is a node
            {
                bully_process MyNode = new bully_process(socket_receive_timeout, my_bully_node_port, ports, the_manager_port);
                MyNode.Start();
            }

            //Second Step: 
            // Socket SS = bully_process.ConnectSocket("127.0.0.1", the_manager_port, true);
            // Console.WriteLine("End of the baset app");
            // Console.WriteLine(SS.ProtocolType.ToString());
            // IPAddress address = IPAddress.Parse("127.0.0.1");
            // listener = new TcpListener(address, the_manager_port);

            // listener.Start();
            // accept = true;

            // Console.WriteLine($"Server started. Listening to TCP clients at 127.0.0.1:{the_manager_port}");
            //// Listen();
        }
        static TcpListener listener;
        static bool accept = false;
        private static void Listen()
        {
            if (listener != null && accept)
            {
                while (true)
                {
                    Console.WriteLine("Waiting for client...");
                }
            }
        }
        private static void ParseArgs(string[] args, out int socket_receive_timeout, out List<int> ports, out int num_of_processes, out int my_bully_node_port, out int the_manager_port)
        {
            //args: in args you can pass the timeout for listening for a coordinator message using option -t
            // and you can list the port numbers that will be used by the processes, these numbers will define two things: number of concurrent processes
            // and the port numbers they will be listening to.
            // In case no args where sent timeout = 1 s and the port numbers will be 10 numbers starting from 50000 up to 50009
            // -n option can be sent to represent number of processes with default port numbers as stated above
            // If option -b or was sent, the next arg will represent the port this node will listen to and this means this instance of app will  manage
            //only one bully process node, in addition to this if -b was sent you need to provide -m which is the manager's port number ( do not confuse manager with coordinator: manager is responsible for interfacing with user where it can send a message to a process to kill itself or receive any logging information to show it to user)
            //First Step: Check on args
            socket_receive_timeout = 0; // 1000 ms = 1 s
            ports = new List<int>();
            num_of_processes = 0;
            my_bully_node_port = -1;
            the_manager_port = -1;
            int option = -1; // 0 = timeout, 1 = number of processes, 2 = ports, 3 = my_bully_node_port, 4 = the_manager_port
            for (int i = 0; i < args.Length; i++)
            {
                if (option == -1 && args[i] != "-t" && args[i] != "-n" && args[i] != "-p" && args[i] != "-b" && args[i] != "-m")
                {
                    //incorrect first argument
                    throw new Exception("Incorrect arguments for the command");

                }
                switch (args[i])
                {
                    case "-t":
                        if (socket_receive_timeout == 0)
                            option = 0;
                        else
                            throw new Exception("Timeout had been set before");
                        break;
                    case "-n":
                        if (num_of_processes == 0)
                            option = 1;
                        else
                            throw new Exception("number of processes had been set before");
                        break;
                    case "-p":
                        option = 2;
                        break;

                    case "-b":
                        if (my_bully_node_port == -1)
                            option = 3;
                        else
                            throw new Exception("the app port had been set before");
                        break;

                    case "-m":
                        if (the_manager_port == -1)
                            option = 4;
                        else
                            throw new Exception("the manager port had been set before");
                        break;
                    default:
                        int number;
                        if (Int32.TryParse(args[i], out number))
                        {
                            switch (option)
                            {
                                case 0:
                                    if (number < 1 || number > 60)
                                        throw new Exception("Allowed timeout must be between 1-60 seconds");
                                    socket_receive_timeout = number;
                                    option = -1;
                                    break;
                                case 1:
                                    if (number < 1 || number > 10)
                                        throw new Exception("Allowed number of concurrent process must be between 1-10");
                                    num_of_processes = number;
                                    option = -1;
                                    break;
                                case 2:
                                    ports.Add(number);
                                    break;
                                case 3:
                                    my_bully_node_port = number;
                                    option = -1;
                                    break;
                                case 4:
                                    the_manager_port = number;
                                    option = -1;
                                    break;
                            }
                        }
                        else
                            throw new Exception("Incorrect arguments for the command");
                        break;
                }
            }
            if (socket_receive_timeout == 0)//Set to default
                socket_receive_timeout = 1;
            if (num_of_processes == 0)
            {
                if (ports.Count > 0)
                    num_of_processes = ports.Count;
                else
                    num_of_processes = 10;
            }
            if (ports.Count == 0)
            {
                for (int i = 0; i < num_of_processes; i++) //Set the default ports
                    ports.Add(50000 + i);
            }
            if (num_of_processes < 1 || num_of_processes > 10)
                throw new Exception("Allowed number of concurrent process must be between 1-10");
            if (socket_receive_timeout < 1 || socket_receive_timeout > 60)
                throw new Exception("Allowed timeout must be between 1-60 seconds");
        }
        public static class OS
        {
            public static bool IsWin() =>
                RuntimeInformation.IsOSPlatform(OSPlatform.Windows);

            public static bool IsMac() =>
                RuntimeInformation.IsOSPlatform(OSPlatform.OSX);

            public static bool IsGnu() =>
                RuntimeInformation.IsOSPlatform(OSPlatform.Linux);

            public static string GetCurrent()
            {
                return
                (IsWin() ? "win" : null) ??
                (IsMac() ? "mac" : null) ??
                (IsGnu() ? "gnu" : null);
            }
        }
    }
}
